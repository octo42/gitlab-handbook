
### Timetable

| Status             | Section                                               | Total Pages | Markdown | ERB | Start      | Finish     |
|:------------------:|:------------------------------------------------------|:-----------:|:--------:|:---:|:----------:|:----------:|
| :white_check_mark: | [Job Families](#job-families)                         | 408         | 408      | 0   | 2023-03-28 | 2023-03-31 |
| :white_check_mark: | [TeamOps](#teamops)                                   | 7           | 0        | 7   | 2023-04-19 | 2023-04-21 |
| :white_check_mark: | [Company Handbook Content](#company-handbook-content) | 52          | 49       | 3   | 2023-05-02 | 2023-05-12 |
| :white_check_mark: | [Tools and Tips](#tools-and-tips)                     | 12          | 12       | 0   | 2023-05-16 | 2023-05-19 |
| :white_check_mark: | [Infrastructure Standards](#infrastructure-standards) | 21          | 21       | 0   | 2023-06-06 | 2023-06-09 |
| :white_check_mark: | [IT Self Service](#it-self-service)                   | 12          | 12       | 0   | 2023-06-27 | 2023-06-30 |
| :white_check_mark: | [Support](#support)                                   | 212         | 208      | 4   | 2023-07-31 | 2023-08-14 |
| :white_check_mark: | [CEO and CoST](#ceo-and-cost)                         | 14          | 12       | 2   | 2023-08-15 | 2023-08-18 |
| :white_check_mark: | [Company](#company)                                   | 161         | 137      | 5   | 2023-08-30 | 2023-09-01 |
| :zap:              | [Security](#security)                                 | 153         | 149      | 3   | 2023-09-05 | 2023-09-08 |
| :zap:              | [Culture](#culture)                                   | 134         | 67       | 45  | 2023-09-18 | 2023-09-21 |
| :zap:              | [Finance](#finance)                                   | 83          | 68       | 16  | 2023-09-26 | 2023-09-29 |
| :zap:              | [Product](#product)                                   | 414         | 351      | 64  | 2023-10-10 | 2023-10-13 |
| :zap:              | [People Group](#people-group)                         | 211         | 192      | 18  | 2023-10-24 | 2023-10-27 |
| :book:             | [Marketing](#marketing)                               | 415         | 385      | 28  | 2023-11-07 | 2023-11-09 |
| :book:             | [Sales](#sales)                                       | 468         | 420      | 44  | 2023-11-20 | 2023-11-22 |
| :book:             | [Legal](#legal)                                       | 67          | 67       | 0   | 2023-12-05 | 2023-12-08 |
| :book:             | [Engineering](#engineering)                           | 810         | 257      | 553 | 2024-01-09 | 2024-02-19 |
| :book:             | [Team](#team)                                         | 3           | 0        | 1   | TBC        | TBC        |

### Status Key

- :zap: - This content will be migrated on the start date indicated
- :book: -  This content is in the planning stages of its migration and the start and finish dates are guides only
- :construction: - This content is in the process of being migrated
- :white_check_mark: - This content has already been migrated to the new handbook

For more details see [the Handbook Roadmap]({{< ref roadmap >}}).
